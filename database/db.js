module.exports = () => {
    "use strict";
    return {
        users: require('./users/users.json'),
        upload: require('./upload/upload.json')
    };
};
