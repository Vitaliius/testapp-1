import { PlayQPage } from './app.po';

describe('play-q App', () => {
  let page: PlayQPage;

  beforeEach(() => {
    page = new PlayQPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
