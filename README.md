# PlayQ

This project was generated with 
[Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.
Also use a json-server:
[json-server](https://github.com/typicode/json-server) version 0.9.6.

## Install Guide
Run `npm install` | `yarn or yarn install`

## Development server 
Run `npm run api` for a json-server with mock data. Navigate to `http://localhost:3000/` (root).
Have 2 endpoint root/users | root/upload

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## All users info:
```
{
    "users": [
        {
            "id":1,
            "name": "testUser 1",
            "email": "test1@gmail.com",
            "password": "1qwe1"
        },
         {
            "id":2,
            "name": "testUser 2",
            "email": "test2@gmail.com",
            "password": "2qwe2"
        },
         {
            "id":3,
            "name": "testUser 3",
            "email": "test3@gmail.com",
            "password": "3qwe3"
        },
         {
            "id":4,
            "name": "testUser 4",
            "email": "test4@gmail.com",
            "password": "4qwe4"
        },
         {
            "id":5,
            "name": "testUser 5",
            "email": "test5@gmail.com",
            "password": "5qwe5"
        },
         {
            "id":6,
            "name": "testUser 6",
            "email": "test6@gmail.com",
            "password": "6qwe6"
        },
         {
            "id":7,
            "name": "testUser 7 ",
            "email": "test7@gmail.com",
            "password": "7qwe7"
        },
         {
            "id":8,
            "name": "testUser 8",
            "email": "test8@gmail.com",
            "password": "8qwe8"
        },
         {
            "id":9,
            "name": "testUser 9",
            "email": "test9@gmail.com",
            "password": "9qwe9"
        },
         {
            "id":10,
            "name": "testUser 10",
            "email": "test10@gmail.com",
            "password": "10qwe10"
        }
    ]
}
```