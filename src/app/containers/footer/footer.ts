import { Component } from '@angular/core';
@Component({
    selector: 'app-footer',
    template: `
        <footer class="footer">
            <div class="footer-copy">&copy;Your Logo Copy msg. 2017</div>
            <div class="footer-link">Your profile link</div>
        </footer>
    `,
    styles: [`
        .footer {
            width: 100%;
            overflow: hidden;
            background: #dadada;
            padding: 15px 0;

            display: flex;
            justify-content: space-between;
            align-items: center; 

            color: #878787;
            font-size: 14px;
            font-family: 'Gloria Hallelujah', cursive;
        }
        .footer-copy {
            margin-left: 10px;
            flex: 1, 0, auto;

        }
        .footer-link {
            margin-right: 10px;
            flex: 1, 0, auto;
        }
    `]
})
// Dummy Container
export class FooterContainer {}
