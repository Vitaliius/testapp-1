import { Component } from '@angular/core';
@Component({
    selector: 'app-header',
    template: `
    <header class="header">
        <div class="header-logo" [routerLink]="['']"><a title="go to home, Johny">Play</a> <b class="logo-q">Q</b></div>
        <ui-navbar class="header-nav"></ui-navbar>
    </header>
    `,
    styles: [`
    .header {
        background: #fafafa;
        width: 100%;
        overflow: hidden;
        padding: 10px 0;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

        display: flex;
        align-items: center;
        justify-content: space-between;
     }
     .header-logo {
         cursor: pointer;
         outline: none;
         margin-left: 10px;
         flex: 0, 0, auto;
         color: #878787;
         font-size: 22px;
         font-family: 'Gloria Hallelujah', cursive;
     }
     .logo-q {color: #CF37A2;}
     .header-nav {
         flex: 1, 0, auto;
     }
    `]
})
// Dummy Container
export class HeaderContainer {}
