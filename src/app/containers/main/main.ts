import { Component } from '@angular/core';
@Component({
    selector: 'app-main',
    template: `
        <main class="main">
            <router-outlet></router-outlet>
        </main>
    `
})
// Dummy Container
export class MainContainer {}

