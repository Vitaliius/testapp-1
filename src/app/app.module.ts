import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
// Containers
import { HeaderContainer } from './containers/header/header';
import { MainContainer } from './containers/main/main';
import { FooterContainer } from './containers/footer/footer';
import { routing } from 'app/app.routing';
import { HomePage } from 'app/pages/home/home';
import { UiNavBar } from './ui/navbar/navbar';
import { UsersService } from './services/users/users.service';
import { LocalStorageService } from './services/localStorage/localstorage.service';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [
    AppComponent,
    // Containers
    HeaderContainer,
    MainContainer,
    FooterContainer,
    HomePage,
    UiNavBar
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [UsersService, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
