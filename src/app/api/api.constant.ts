// Declare our const url for request
const root = 'http://localhost:3000';
export const API_CONST = {
    // provide shorthand acess to url/specific
    users: root + '/users',
    upload: root + '/upload'
};
