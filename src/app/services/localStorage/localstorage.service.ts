import { Injectable } from '@angular/core';
import { IUsers } from '../users/Iusers';
@Injectable()
export class LocalStorageService {
    /**
     * @local = shorthand for localStorage object
     */
    public local = window.localStorage;

     /**
     * @name = we pass from app.component.ts (when we get it from mock server)
     * before save we convert it to 'key' 'json object'
     */
    setLocal(name, data) {
        this.local.setItem(name, JSON.stringify(data));
    }

     /**
     * get data from localStorage and parse to normal js object
     */
    getLocal(name) {
        return this.local.getItem(name);
    }

    /**
     * clear method for key => 'name' in localStorage
     */
    clearLocal(name) {
        this.local.removeItem(name);
        this.local.clear();
    }
}
