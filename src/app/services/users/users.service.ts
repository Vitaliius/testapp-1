import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { API_CONST } from '../../api/api.constant';
import { LocalStorageService } from '../localStorage/localstorage.service';

@Injectable()
export class UsersService {
    public user$;
    constructor(private http: Http, private local: LocalStorageService) {}
    // Make this data available for repeated requests all our users data
    getUsers() {
       return this.http.get(API_CONST.users)
            .map(res => res.json());
    }
    // User instance in localStorage
    getUser(name) {
        return this.user$ = new Observable(observer => {
            observer.next(JSON.parse(this.local.getLocal(name)));
        });
    }

}
