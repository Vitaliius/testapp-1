export interface IUpload {
    id: number;
    ImageName: string;
    fileSize: string;
    checksum: string;
    uploadUser: number;
}
