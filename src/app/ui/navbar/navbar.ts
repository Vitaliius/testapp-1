import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users/users.service';
@Component({
    selector: 'ui-navbar',
    templateUrl: './navbar.html',
    styleUrls: ['./navbar.css']
})

export class UiNavBar implements OnInit {
    public user: {} = {};
    public ifUserExist: boolean;
    constructor(private userService: UsersService) {}
    ngOnInit(): void {
        this.getUserLocal();
    }
    getUserLocal() {
        this.userService.getUser('user')
            .subscribe(
                data => {
                    this.user = data;
                    this.ifExist(this.user);
                },
                err => console.log('checkIfUserExist has error: ', err)
            );
    }

    ifExist(user) {
        user !== null ? this.ifUserExist = true : this.ifUserExist = false;
    }
}
