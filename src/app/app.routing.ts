import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './pages/home/home';

const routes: Routes = [
  { path: '', component: HomePage  },
  { path: 'sign', loadChildren: 'app/lazy_components/sign/sign.module#SignModule'},
  { path: 'view', loadChildren: 'app/lazy_components/view/view.module#ViewModule'},
  { path: 'upload', loadChildren: 'app/lazy_components/upload/upload.module#UploadModule'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
