import { Component, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { IUsers } from './services/users/Iusers';
import { UsersService } from './services/users/users.service';
import { LocalStorageService } from './services/localStorage/localstorage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(private http: Http, private userService: UsersService, private local: LocalStorageService) {}
  ngOnInit(): void {
    this.requestJSON();
  }
  /**
   *  step 1 | requestJSON() need to get mock data from json-server
   *  step 2 | and after we store this data to a local storage use setMockData()
   *  step 3 | when user "leave tab - trigger ngOnDestroy and clear all localStorage"
   */
  // step 1
  requestJSON() {
    this.userService.getUsers()
      .subscribe(
        data => {
          this.setMockData(data.users);
        },
        err => console.log(err)
      );
  }
  // step 2
  setMockData(users: IUsers[]) {
    this.local.setLocal('users', users);
  }
  // step 3
  ngOnDestroy() {
    this.local.clearLocal('users');
    this.local.clearLocal('user');
  }

}
