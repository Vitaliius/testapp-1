import { Component } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
@Component({
    templateUrl: 'block.html',
    styleUrls: ['block.css'],
})
export class BlockComponent {
    public user;
    public uploads = [
        {
            id: 1,
            imageName: 'somename1',
            fileSize: '144.6kb',
            checksum: 'sha2412314',
            uploadedUser: 1
        },
        {
            id: 2,
            imageName: 'somename2',
            fileSize: '544.6kb',
            checksum: 'sha2314',
            uploadedUser: 2
        },
        {
            id: 3,
            imageName: 'somename3',
            fileSize: '344.6kb',
            checksum: 'sha144314',
            uploadedUser: 3
        }
    ];
    constructor(private usersService: UsersService) {}
    getUserLocal() {
       this.usersService.getUser('user')
        .subscribe(
            data => this.user = data,
            err => console.log(err)
        );
    }

    ifCancel(e: Event, item) {
        for (let i = this.uploads.length; i--; ) {
            this.uploads[i].id === item.id ? this.uploads.splice(i, 1) : console.log(this.uploads);
        }
    }
}
