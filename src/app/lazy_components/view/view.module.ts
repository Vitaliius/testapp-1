import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ViewComponent } from './view';
import { TableComponent } from './table/table';
import { BlockComponent } from './block/block';
import { viewRouting } from './view.routing';
import { ImgHoverDirective } from '../../directives/hover/hover';

@NgModule({
  imports: [CommonModule, viewRouting],
  declarations: [ViewComponent, TableComponent, BlockComponent, ImgHoverDirective]
})
export class ViewModule {}
