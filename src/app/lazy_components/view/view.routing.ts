import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './view';
import { BlockComponent } from './block/block';
import { TableComponent } from './table/table';

const routes: Routes = [
  { path: '', component: ViewComponent,
    children: [
        {path: 'block', component: BlockComponent},
        {path: 'table', component: TableComponent}
    ]}
];

export const viewRouting: ModuleWithProviders = RouterModule.forChild(routes);
