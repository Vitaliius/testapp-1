import { Component, ElementRef, Renderer2  } from '@angular/core';
@Component({
    templateUrl: 'table.html',
    styleUrls: ['table.css'],
})
export class TableComponent {
    public tableUploads = [
        {
            id: 1,
            imageName: 'somename1',
            fileSize: '144.6kb',
            checksum: 'sha2412314',
            uploadedUser: 1
        },
        {
            id: 2,
            imageName: 'somename2',
            fileSize: '544.6kb',
            checksum: 'sha2314',
            uploadedUser: 2
        },
        {
            id: 3,
            imageName: 'somename3',
            fileSize: '344.6kb',
            checksum: 'sha144314',
            uploadedUser: 3
        }
    ];

    constructor(private el: ElementRef, private renderer: Renderer2) {}
    ifView(e, img) {
        console.log(img.id);
        const aim = this.el.nativeElement.querySelector('.modal');
        // const target = e.target.nextElementSibling;
        this.renderer.setStyle(aim, 'display', 'block');
    }

    ifDelete(id: number) {
        for (let i = this.tableUploads.length; i--; ) {
            this.tableUploads[i].id === id ? this.tableUploads.splice(i, 1) : console.log(this.tableUploads);
        }
    }
    closeModal(e) {
        const aim = this.el.nativeElement.querySelector('.modal');
        // const target = e.target.nextElementSibling;
        this.renderer.setStyle(aim, 'display', 'none');
    }
}
