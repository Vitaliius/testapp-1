import { API_CONST } from '../../api/api.constant';
import { Component, OnInit, NgZone, Inject, EventEmitter  } from '@angular/core';
import { Http } from '@angular/http';
import { NgUploaderOptions } from 'ngx-uploader';
import { LocalStorageService } from '../../services/localStorage/localstorage.service';
const url = API_CONST.upload;
@Component({
    templateUrl: 'upload.html',
    styleUrls: ['upload.css'],
})
// I need more learn angular 2 if i want to create image file uploader by my self )) thx
// Nice task
export class UploadComponent implements OnInit {
    public options: NgUploaderOptions;
    public response: any;
    public sizeLimit: number = 1000000; // 1MB
    public previewData: any;
    public errorMessage: string;
    public inputUploadEvents: EventEmitter<string>;

    constructor(@Inject(NgZone) private zone: NgZone, private local: LocalStorageService ) {
        this.options = new NgUploaderOptions({
            filterExtensions: true,
            allowedExtensions: ['jpg', 'jfif', 'png'],
            maxSize: 2097152,
            data: {
                id: 3,
                imageName: 'somenname3',
                fileSize: '144.5kb3',
                checksum: 'SHA2563',
                uploadedUser: 3
             },
            url: null,
            autoUpload: false,
            fieldName: 'images',
            fieldReset: true,
            maxUploads: 2,
            // method: 'POST',
            previewUrl: true,
            withCredentials: false
        });
        this.inputUploadEvents = new EventEmitter<string>();
    }

    ngOnInit(): void {
        this.overWriteLocal();
    }

    startUpload() {
        this.inputUploadEvents.emit('startUpload');
    }
    overWriteLocal() {
        const oldUserData = JSON.parse(this.local.getLocal('user'));
        const newData = {
            id: 3,
            imageName: 'somenname3',
            fileSize: '144.5kb3',
            checksum: 'SHA2563',
            uploadedUser: 3
        };
        oldUserData['images'] = newData;
        this.local.setLocal('user', oldUserData);
    }

    beforeUpload(uploadingFile): void {
        if (uploadingFile.size > this.sizeLimit) {
        uploadingFile.setAbort();
        this.errorMessage = 'File is too large!';
        }
    }

    handleUpload(data: any) {
        setTimeout(() => {
        this.zone.run(() => {
            this.response = data;
            if (data && data.response) {
            this.response = JSON.parse(data.response);
            }
        });
        });
    }

    handlePreviewData(data: any) {
        this.previewData = data;
    }
    closePrevieImage(e: Event): void {
        console.log('Preview Image Close: ', e);
    }
    clearAll(e: Event): void {
        console.log('Clear All: ', e);
    }

    // saveUpload(e: Event): void {
    //     console.log('save Upload: ', e);
    // }
}
