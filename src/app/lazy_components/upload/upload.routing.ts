import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './upload';

const routes: Routes = [
  { path: '', component: UploadComponent}
];

export const uploadRouting: ModuleWithProviders = RouterModule.forChild(routes);
