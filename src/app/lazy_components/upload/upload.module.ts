import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UploadComponent } from './upload';
import { uploadRouting } from './upload.routing';
import { NgUploaderModule } from 'ngx-uploader';

@NgModule({
  imports: [CommonModule, uploadRouting, NgUploaderModule],
  declarations: [UploadComponent]
})
export class UploadModule {}
