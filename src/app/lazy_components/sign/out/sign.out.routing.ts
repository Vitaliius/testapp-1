import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignOutComponent } from './sign.out';

const routes: Routes = [
  { path: '', component: SignOutComponent},

];

export const signOutRouting: ModuleWithProviders = RouterModule.forChild(routes);
