import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignOutComponent } from './sign.out';
import { signOutRouting } from './sign.out.routing';

@NgModule({
  imports: [CommonModule, signOutRouting],
  declarations: [SignOutComponent]
})
export class SignOutModule {}
