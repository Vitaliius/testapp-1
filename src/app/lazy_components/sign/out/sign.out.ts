import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../../services/users/users.service';
import { LocalStorageService } from '../../../services/localStorage/localstorage.service';
@Component({
    template: `
        <div class="sign-out">
            <div class="data-out">
                <p>NAME: <span>{{user?.name}}</span></p>
                <p>EMAIL: <span>{{user?.email}}</span></p>
                <p>PASSWORD: <span>{{user?.password}}</span></p>
                <div class="btn-group">
                    <button class="out-btn stay">Stay</button> 
                    <button class="out-btn out">Sign Out</button>
                </div>
            </div>
        </div>
    `,
    styles: [`
        .sign-out {
            display: flex;
            justify-content: center;
            width: 23%;
            background: #dadada;
            padding: 15px 0;
            margin: 75px auto 50px auto;
        }
        .data-out {
            width: 85%;
        }
        p {margin-bottom: 10px;}
        span { float: right;}
        .btn-group {
            margin-top: 15px;
            display: flex;
            justify-content: space-between;
        }
        .out-btn {
            width: 40%;
        }
    `]
})

export class SignOutComponent implements OnInit {
    public user;
    constructor(private local: LocalStorageService, private userService: UsersService, private router: Router) {}
    ngOnInit(): void {
        // Check for sign in user at localstorage
        this.getUserLocal();
    }

    getUserLocal() {
         this.userService.getUser('user')
            .subscribe(
                data => this.user = data,
                err => console.log('getUserLocal has error ', err)
                );
    }
    // Listen for events
    @HostListener('click', ['$event'])
    onClick(e) {
         switch (e.target.className) {
             case 'out-btn stay':
                 this.router.navigate(['view/block']);
                 break;
            case 'out-btn out':
                 this.local.clearLocal('user');
                 this.router.navigate(['sign/in']);
                 break;
             default:
                console.log('ESLE');
                 break;
         }
    }
}
