import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignComponent } from './sign';

const routes: Routes = [
  { path: '', component: SignComponent},
  { path: 'in', loadChildren: 'app/lazy_components/sign/in/sign.in.module#SignInModule'},
  { path: 'out', loadChildren: 'app/lazy_components/sign/out/sign.out.module#SignOutModule'}
];

export const signRouting: ModuleWithProviders = RouterModule.forChild(routes);
