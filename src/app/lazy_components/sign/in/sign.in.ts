import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IUsers } from '../../../services/users/Iusers';
import { LocalStorageService } from '../../../services/localStorage/localstorage.service';
import { UsersService } from '../../../services/users/users.service';
@Component({
    templateUrl: 'sign.in.html',
    styleUrls: ['sign.in.css'],
})
export class SignInComponent implements OnInit {
    public localUsers;
    public localUser: any = {};
    public emailRegex = '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$';
    // Create a form
    public userForm = new FormGroup({
        email: new FormControl('', [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(32),
            Validators.pattern(this.emailRegex)
        ]),
        password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(16)])
    });

    constructor(private local: LocalStorageService, private route: Router, private userService: UsersService) {}

    ngOnInit(): void {
        // Get all users data from localStorage
        this.requestUsers();
    }

    requestUsers() {
        this.userService.getUsers()
            .subscribe(
                value => this.localUsers = value,
                err => console.log('requestUsers has error: ', err)
            );
    }

    // Cacth data from submit form
    userFormSubmit(user) {
        const checkUser = user.value;
        const copy = [...this.localUsers.users];
        for (const item of copy) {
            if (item.email === checkUser.email && item.password === checkUser.password) {
               this.ifExist(item);
            } else {
                 console.log('No suck user here'); // Will display contents of the object inside the array
            }
        }
    }

    // Check if user data correct sign in user and save to localStorage
    ifExist(profile) {
        console.log('profile in ifExist: ', profile);
        this.local.setLocal('user', profile);
        this.route.navigate(['view/block'], profile);
    }
    // helper method Clear localstorage from user data
    initClear() {
        this.local.clearLocal('users');
    }
}
