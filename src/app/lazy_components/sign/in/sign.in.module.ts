import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module
import { SignInComponent } from './sign.in';
import { signInRouting } from './sign.in.routing';
import { LocalStorageService } from '../../../services/localStorage/localstorage.service';

@NgModule({
  imports: [CommonModule, signInRouting, ReactiveFormsModule],
  declarations: [SignInComponent],
  providers: [LocalStorageService]
})
export class SignInModule {}
