import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignComponent } from './sign';
import { signRouting } from './sign.routing';
import { UsersService } from '../../services/users/users.service';

@NgModule({
  imports: [CommonModule, signRouting],
  declarations: [SignComponent],
  providers: [UsersService]
})
export class SignModule {}
